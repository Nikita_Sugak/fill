﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fill
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            bitmap = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            g = Graphics.FromImage(bitmap);
        }

        Graphics g;
        Bitmap bitmap;
        Color black, red;
        List<Pixel> list = new List<Pixel>();
        Pixel p, pnew;
        Color cvet;
        Brush brush;

        bool DrowClic = false;
        private void button2_Click(object sender, EventArgs e)
        {
            brush = Brushes.Black;
            switch (DrowClic)
            {
                case false:
                    DrowClic = true;
                break;
                case true:
                    DrowClic = false;
                break;
            }
        }

        bool Drow;
        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (Drow && DrowClic)
            {
                g.FillRectangle(Brushes.Black, e.X, e.Y, 5, 5);
                pictureBox1.Image = bitmap;
            }
        }

        private void Fill()
        {
            g.FillRectangle(brush, pnew.x, pnew.y, 1, 1);
            pictureBox1.Image = bitmap;
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            Drow = true;
            black = Color.Black;
            red = Color.Red;
            brush = Brushes.Red;
            if (getClic)
            {
                p = new Pixel(e.X, e.Y);
                list.Add(p);
                do
                {
                    pnew = list[list.Count - 1];
                    list.RemoveAt(list.Count - 1);
                    Fill();
                    Add(pnew.x + 1, pnew.y);
                    Add(pnew.x, pnew.y - 1);
                    Add(pnew.x - 1, pnew.y);
                    Add(pnew.x, pnew.y + 1);
                } while (list.Count != 0);
            }
        }

        private void Add(int x, int y)
        {
            cvet = ((Bitmap)pictureBox1.Image).GetPixel(x, y);
            if ((cvet.A != black.A || cvet.R != black.R || cvet.G != black.G || cvet.B != black.B) && 
                (cvet.A != red.A || cvet.R != red.R || cvet.G != red.G || cvet.B != red.B))
            {
                p = new Pixel(x, y);
                list.Add(p);
            }
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            Drow = false;
        }

        bool getClic = false;
        private void button3_Click(object sender, EventArgs e)
        {
            DrowClic = false;
            switch (getClic)
            {
                case false:
                    getClic = true;
                    break;
                case true:
                    getClic = false;
                    break;
            }
        }
    }

    public class Pixel
    {
        public int x;
        public int y;

        public Pixel(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }
}